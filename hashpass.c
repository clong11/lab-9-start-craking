
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

char *md5(const char *str, int length);

//takes filename as first argument, output filename as the second argument
int main(int argc, char *argv[]) {
    FILE * inFile;
    FILE * outFile;
    char *inFileName = argv[1];
    char *outFileName = argv[2];
    if(inFileName == NULL || outFileName == NULL){
        printf("input file or output file not specified. exiting..\n");
        exit(0);
    }

    inFile = fopen(inFileName,"r");
    outFile = fopen(outFileName,"w");
    
    
    char buffer[15];
    
    //run through each line of the file
    while(fgets(buffer, 128, inFile)) {
        printf("%s is %lu letters long\n",buffer,strlen(buffer)-1);
        printf("%s\n",md5(buffer, strlen(buffer)-1));
        fwrite(md5(buffer, strlen(buffer)-1),1,strlen(md5(buffer, strlen(buffer))),outFile);
        fwrite("\n",1,1,outFile);
        
        
    }
        printf("\n");

    fclose(inFile);
    return 0; 

}

